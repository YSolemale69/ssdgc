<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <head>
    <?php
		include_once '../vue/template/header.php';
	?>

        <script src="/ssdgc/vue/css/bootstrap/js/bootstrap.min.js"></script>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript">
        	jQuery.fn.multiselect = function() {
			    $(this).each(function() {
			        var checkboxes = $(this).find("input:checkbox");
			        checkboxes.each(function() {
			            var checkbox = $(this);
			            // Highlight pre-selected checkboxes
			            if (checkbox.prop("checked"))
			                checkbox.parent().addClass("multiselect-on");
			 
			            // Highlight checkboxes that the user selects
			            checkbox.click(function() {
			                if (checkbox.prop("checked"))
			                    checkbox.parent().addClass("multiselect-on");
			                else
			                    checkbox.parent().removeClass("multiselect-on");
			            });
			        });
			    });
			};

			
        </script>

		<title>lien</title>
</head>

<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-8">
			<?php
				include_once '../vue/template/menu.php';
			?>
		</div>
	</div>
	<div class="row justify-content-center">		
		<body>
			<div class="col-2">  
				<br>
				<form method="post" action="" id="formlien" enctype="multipart/form-data">
		    		<fieldset>
		        		<legend>Ajouter un lien</legend>

		                	<p><input type="text" class="form-control" name="titre"  placeholder="Entrez un titre pour votre lien" required><br></p>
                            <p><input type="text" class="form-control" name="desc"  placeholder="Entrez votre lien" required></input><br></p>
                            	<div class="multiselect">
                            		<?php 
                            			foreach ($categorie as $v1 => $v2) 
			                            {
											echo "<label><input type='checkbox' name='option[]' value='".$v2['id']."' />".utf8_encode($v2['nom'])."</label>";
			                            }

                            		?>
								</div>
                            </p>
		                	<p><button type="submit" class="btn btn-success pull-left">Envoyer <i class="icon-white icon-ok-sign"></i></button>

			        </fieldset>
				</form>

			</div>
		</body>
	</div>
</div>

</html>