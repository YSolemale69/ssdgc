<?php
class Links
{
	public static function set_link($user_id, $lien_nom, $url)
    {
        global $bdd;

        $req = $bdd->prepare ('
            INSERT INTO links(user_id, lien_nom, url) 
            VALUES(:user_id, :lien_nom, :url)');
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $req->bindParam(':lien_nom', $lien_nom, PDO::PARAM_STR, 500);
        $req->bindParam(':url', $url, PDO::PARAM_STR, 500);
        $req->execute();
    }

    public static function delete_link($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            DELETE FROM links 
            WHERE id = :id');
        $req->bindParam(':id', $id,PDO::PARAM_INT);
        $req->execute();
    }

    public static function update_link($id, $user_id, $lien_nom, $url)
    {
        global $bdd;

        $req = $bdd->prepare('
            UPDATE links 
            SET lien_nom = :lien_nom, url = :url 
            WHERE id = :id 
            AND user_id = :user_id');
        $req->bindParam(':lien_nom', $lien_nom, PDO::PARAM_STR, 500);
        $req->bindParam(':url', $url, PDO::PARAM_STR, 500);
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $req->execute();
    }

    public static function get_all_links()
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, user_id, lien_nom, url, date 
            FROM links');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_last_links_for_public()
    {
        global $bdd;

        $req = $bdd->prepare("
            SELECT id, user_id, lien_nom, url, date, GROUP_CONCAT(nom  SEPARATOR ', ') AS categorie_nom 
            FROM (SELECT a.*, c.nom  
            FROM links a 
            INNER JOIN categories_relation b 
            ON a.id = b.id_lien 
            INNER JOIN categories c 
            ON b.id_categorie = c.id) x             
            GROUP BY id, user_id, lien_nom, url, date            
            ORDER BY date DESC LIMIT 5");
        $req->execute();
        return $req->fetchAll();
    }

    public static function get_links_by_user_id($user_id)
    {
        global $bdd;

        $req = $bdd->prepare("
            SELECT id, user_id, lien_nom, url, date, GROUP_CONCAT(nom  SEPARATOR ', ') AS categorie_nom 
            FROM (SELECT a.*, c.nom  
            FROM links a 
            INNER JOIN categories_relation b 
            ON a.id = b.id_lien 
            INNER JOIN categories c 
            ON b.id_categorie = c.id 
            WHERE user_id = :user_id) x
            GROUP BY id, user_id, lien_nom, url, date");
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);        
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_links_by_id($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, user_id, lien_nom, url, date 
            FROM links 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->execute();

        return $req->fetch();
    }

    public static function get_links_by_url($url)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, user_id, lien_nom, url, date 
            FROM links 
            WHERE url = :url');
        $req->bindParam(':url', $url, PDO::PARAM_STR, 500);
        $req->execute();

        return $req->fetch();
    }

    public static function get_links_by_lien_nom($lien_nom)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, user_id, lien_nom, url, date 
            FROM links 
            WHERE lien_nom = :lien_nom');
        $req->bindParam(':lien_nom', $lien_nom, PDO::PARAM_STR, 500);
        $req->execute();

        return $req->fetch();
    }
}