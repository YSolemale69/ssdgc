<!DOCTYPE html>
<html>
    <head>
    <?php
		include_once '../vue/template/header.php';
	?>

		<title>404</title>
</head>

<div class="container-fluid">

	<body>
		<div class="row-fluid">
			<div class="span8 offset2">
				<?php
				include_once '../vue/template/menu.php';
				?>
				<div class="span3 offset5"> 
					Oups ... la page demandées n'existe pas
				</div>
			</div>
		</div>
	</body>

</div>

</html>