<?php
session_start();
if (isset($_SESSION['id']))
{
	//sécurisation de l'id
	$_SESSION['id'] = htmlspecialchars(intval($_SESSION['id']));

	header("location:../../index.php");
}

// session_destroy();
$var_dir = "../../";
if (isset($_POST['nom'])) 
{
    include_once('../../modele/connexion_sql.php');
    include_once('../../modele/User.php');
    $strp = strlen($_POST['password']);

    //mot de passe différents
	if ($_POST['password'] != $_POST['confirmation']) 
	{
		$div_type = "error";
		$erreur_type = "Erreur !";
		$erreur = "Les mots de passe ne correspondent pas";
	}
	//mot de passe trop court
	elseif ($strp < 5) 
	{
		$div_type = "error";
		$erreur_type = "Erreur !";
		$erreur = "Votre mot de passe est trop court, minimum 6 caractères";
	}
	else
	{
        $user_count = User::get_user_by_email($_POST['email']);

        //email déjà utilisé
        if ($user_count > 0) 
        {
        	$div_type = "error";
			$erreur_type = "Erreur !";
			$erreur = "Cette adresse mail est déjà utilisée";
        }
        else
        {
        	//sécurisation des données du formulaire
		    $_POST['nom'] = htmlspecialchars($_POST['nom']);
		    $_POST['prenom'] = htmlspecialchars($_POST['prenom']);
		    $_POST['username'] = htmlspecialchars($_POST['username']);
		    $_POST['email'] = htmlspecialchars($_POST['email']);
		    $_POST['password'] = htmlspecialchars($_POST['password']);
		    $hashed_password = password_hash($_POST['password'], PASSWORD_DEFAULT);

            $user = User::set_user($_POST['nom'], $_POST['prenom'], $_POST['username'], $_POST['email'], $hashed_password);

        	$div_type = "success";
			$erreur_type = "Félicitation !";
			$erreur = "Votre compte a bien été créé, <a href='login'>Se connecter</a>";

            $user_count = User::get_user_by_email_password($_POST['email'], $hashed_password);
            $_SESSION['id'] = $user_count['id'];
            $_SESSION['email'] = $user_count['email'];
            $_SESSION['username'] = $user_count['username'];
            $_SESSION['type'] = $user_count['type'];
            $_SESSION['register'] = true;
            //header("location:openid.php");
        }
	}
	include_once '../../vue/auth/register.php';
}
else
{
	include_once '../../vue/auth/register.php';
}