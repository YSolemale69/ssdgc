<?php
session_start();
if (!isset($_SESSION['id']))
{
	header("location:../index.php");
}

$var_dir = "../";
include_once('../modele/connexion_sql.php');

//sécurisation de l'id
$_SESSION['id'] = htmlspecialchars(intval($_SESSION['id']));

//récupération des catégories pour affichage de lien
include_once('../modele/Categorie.php');
$categorie = Categorie::get_all_categories();

if (isset($_POST['titre']) && isset($_POST['desc'])) 
{
	//sécurisation des données du formulaire
    $_SESSION['id'] = htmlspecialchars($_SESSION['id']);
    $_POST['titre'] = htmlspecialchars($_POST['titre']);
    $_POST['desc'] = htmlspecialchars($_POST['desc']);

	//on créé l'entrée du lien dans la DB
	include_once('../modele/Links.php');
	$set_link = Links::set_link($_SESSION['id'],$_POST['titre'], $_POST['desc']);
	
	//on ajoute les catégories
	$lien_id = Links::get_links_by_lien_nom($_POST['titre']);
	foreach ($_POST['option'] as $v1 => $v2) 
    {
    	$new_lien_categorie = Categorie::set_categorie_relation($lien_id['id'],$v2);
    	echo $new_lien_categorie;
    }
	
	include_once '../vue/liens/lien.php';
}

else
{
	include_once '../vue/liens/lien.php';
}