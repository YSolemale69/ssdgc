<!DOCTYPE html>
<html>
    <head>
    <?php
		include_once '../../vue/template/header.php';
	?>

		<title>Connexion</title>
</head>

<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-8">
			<?php
				include_once '../../vue/template/menu.php';
			?>
		</div>
	</div>
	<div class="row justify-content-center">		
		<body>
			<div class="col-2">  
				<br>
				<!-- formulaire de connexion  -->
				<form class="form-group" method="post" action="" id="formLogin">
				    <fieldset>
				        <legend>Connexion</legend>
				            <div class="control-group">
				                <div class="controls">

				                
				            	<!--   affichage des messages d'erreur  -->
				                <?php 
				                	$var=0;
				                	if (isset($erreur)) 
				                	{
				                		echo '<div class="alert alert-'.$div_type.'">
						                    <h4 class="alert-heading">'.$erreur_type.'</h4>
						                    '.$erreur.'</div>';
				                	}
				                ?>
				                 

				                <p><input type="email" class="form-control" name="email"  placeholder="Email" required><br></p>
				            
				                <p><input type="password" class="form-control" name="password"  placeholder="Mot de passe" required><br></p> 
				            

				                <p><button type="submit" class="btn btn-success pull-left">Se connecter <i class="icon-white icon-ok-sign"></i></button>
				                <a href="register.php">&nbsp Pas encore enregistré ?</a></p>
				                
				            	</div>
				        	</div>
				    </fieldset>
				</form>
                <!-- <a href="<?php echo $openid_url; ?>">Login with Google</a> -->
			</div>
		</div>
	</body>

</div>


</html>