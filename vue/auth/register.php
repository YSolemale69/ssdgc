<!DOCTYPE html>
<html>
    <head>
    <?php
		include_once '../../vue/template/header.php';
	?>

		<title>Enregistrement</title>
</head>

<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-8">
			<?php
				include_once '../../vue/template/menu.php';
			?>
		</div>
	</div>
	<div class="row justify-content-center">		
		<body>
			<div class="col-2"> 
				<br>
				<!-- formulaire d'enregistrement  -->
				<form method="post" action="" id="formRegister">
				    <fieldset>
				        <legend>Register</legend>
				            <div class="control-group">
				                <div class="controls">

				                
				            	<!--   affichage des messages d'erreur  -->
				                <?php 
				                	$var=0;
				                	if (isset($erreur)) 
				                	{
				                		echo '<div class="alert alert-'.$div_type.'">
						                    <h4 class="alert-heading">'.$erreur_type.'</h4>
						                    '.$erreur.'</div>';
				                	}
				                ?>

				                <p><input type="text" class="form-control" class="input-xlarge" name="nom"  placeholder="Nom" required><br></p>

				                <p><input type="text" class="form-control" class="input-xlarge" name="prenom"  placeholder="Prénom" required><br></p>

   				                <p><input type="text" class="form-control" name="username" placeholder="Nom d'utilisateur" required><br></p>

				                <p><input type="email" class="form-control" name="email"  placeholder="Email" required><br></p>
				            
				                <p><input type="password" class="form-control" name="password"  placeholder="Mot de passe" required><br></p> 
				            
				                <p><input type="password" class="form-control" name="confirmation"  placeholder="Confirmer votre mot de passe" required><br></p>

				                <p><button type="submit" class="btn btn-success pull-left">S'enregistrer <i class="icon-white icon-ok-sign"></i></button>
				                <a href="login.php">&nbspDéjà enregistré ?</a></p>
				                
				            	</div>
				        	</div>
				    </fieldset>
				</form>

			</div>
		</div>
	</body>

</div>


</html>