<?php
class Video
{
     public static function get_popular_video()
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT nom, titre, thumbnail, vue 
            FROM video 
            WHERE confidentialite = 0 OR confidentialite = 2 
            ORDER BY vue DESC 
            LIMIT 5');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_popular_video_for_public()
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT nom, titre, thumbnail, vue 
            FROM video 
            WHERE confidentialite = 0 
            ORDER BY vue DESC 
            LIMIT 5');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_last_video()
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT nom, titre, thumbnail, vue 
            FROM video 
            WHERE confidentialite = 0 OR confidentialite = 2 
            ORDER BY date DESC 
            LIMIT 5');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_last_video_for_public()
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT nom, titre, thumbnail, vue 
            FROM video 
            WHERE confidentialite = 0 
            ORDER BY date DESC 
            LIMIT 5');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_video_by_id($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, user_id, src, thumbnail, nom, titre, description, confidentialite 
            FROM video 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->execute();

        return $req->fetch();
    }

    public static function get_video_by_name($nom)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, user_id, src, thumbnail, nom, titre, description, confidentialite 
            FROM video 
            WHERE nom = :nom');
        $req->bindParam(':nom', $nom, PDO::PARAM_STR, 500);
        $req->execute();

        return $req->fetch();
    }

    public static function get_video_by_user_id($user_id)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom, titre, thumbnail, vue 
            FROM video 
            WHERE user_id = :user_id');
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $req->execute();

        return $req->fetchAll();
    }

    public static function set_video($user_id, $src, $thumbnail, $nom, $titre, $desc, $conf)
    {
        global $bdd;

        $req = $bdd->prepare ('
            INSERT INTO video(user_id, src, thumbnail, nom, titre, description, confidentialite) 
            VALUES(:user_id, :src, :thumbnail,:nom, :titre, :desc, :conf)');
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $req->bindParam(':src', $src, PDO::PARAM_STR, 500);
        $req->bindParam(':thumbnail', $thumbnail, PDO::PARAM_STR, 500);
        $req->bindParam(':nom', $nom, PDO::PARAM_STR, 500);
        $req->bindParam(':titre', $titre, PDO::PARAM_STR, 255);
        $req->bindParam(':desc', $desc, PDO::PARAM_STR, 2000);
        $req->bindParam(':conf', $conf, PDO::PARAM_INT);
        $req->execute();
    }

    public static function update_video($id, $user_id, $title, $desc, $conf)
    {
        global $bdd;

        $req = $bdd->prepare('
            UPDATE video 
            SET titre = :titre, description = :desc, confidentialite = :conf 
            WHERE id = :id 
            AND user_id = :user_id');
        $req->bindParam(':titre',$title,PDO::PARAM_STR,255);
        $req->bindParam(':desc',$desc);
        $req->bindParam(':id', $id,PDO::PARAM_INT);
        $req->bindParam(':user_id',$user_id,PDO::PARAM_INT);
        $req->bindParam(':conf',$conf,PDO::PARAM_INT);
        $req->execute();
    }

    public static function update_view($nom, $vues){
        global $bdd;

        $vues++;
        $req = $bdd->prepare('
            UPDATE video 
            SET vue = :vue 
            WHERE nom = :nom');
        $req->bindParam(':nom', $nom, PDO::PARAM_STR, 500);
        $req->bindParam(':vue', $vues,PDO::PARAM_INT);
        $req->execute();
    }

    public static function delete_video($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            DELETE FROM video 
            WHERE id = :id');
        $req->bindParam(':id', $id,PDO::PARAM_INT);
        $req->execute();
    }

    public static function delete_all_user_video($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            DELETE FROM video 
            WHERE user_id = :id');
        $req->bindParam(':id', $id,PDO::PARAM_INT);
        $req->execute();
    }
}