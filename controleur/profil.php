<?php
session_start();
if (!isset($_SESSION['id']))
{
    header("location:auth/login.php");
}

$var_dir = "../";
include_once('../modele/connexion_sql.php');

//sécurisation de l'id
$_SESSION['id'] = htmlspecialchars(intval($_SESSION['id']));

//récupération des informations utilisateur
include_once('../modele/User.php');
$user = User::get_user_by_id($_SESSION['id']);

//récupération des catégories pour affichage modification de lien
include_once('../modele/Categorie.php');
$categorie = Categorie::get_all_categories();

//récupération des liens de l'utilisateur
include_once('../modele/Links.php');
$links = Links::get_links_by_user_id($_SESSION['id']);


//suppression de lien
if (isset($_POST['lienIdSuppr'])) 
{
    //sécurisation de l'id
    $_SESSION['id'] = htmlspecialchars(intval($_SESSION['id']));

    //on supprime l'entrée du lien dans la DB    
    $delete_link = Links::delete_link($_POST['lienIdSuppr']);
    
    //on supprime les catégories                
    $delete_lien_categorie = Categorie::delete_categorie_relation($_POST['lienIdSuppr']);            
    
    //récupération de la liste de liens
    $links = Links::get_links_by_user_id($_SESSION['id']);

    include_once '../vue/profil.php';
}

//modification de lien
//ToDo : modification de catégories
elseif (isset($_POST['lienIdEdit'])) 
{
    //sécurisation des données du formulaire
    $_POST['lienIdEdit'] = htmlspecialchars(intval($_POST['lienIdEdit']));
    $_SESSION['id'] = htmlspecialchars(intval($_SESSION['id']));
    $_POST['lien_nom'] = htmlspecialchars($_POST['lien_nom']);
    $_POST['url'] = htmlspecialchars($_POST['url']);

    //mise à jour de la liste de liens
    $links = Links::update_link($_POST['lienIdEdit'], $_SESSION['id'], $_POST['lien_nom'], $_POST['url']);
    
    //récupération de la liste de liens
    $links = Links::get_links_by_user_id($_SESSION['id']);

    include_once '../vue/profil.php';
}

include_once '../vue/profil.php';