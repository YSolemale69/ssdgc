<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="icon" type="image/png" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>vue/css/img/favicon.ico" />
<link rel="stylesheet" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>vue/css/style.css" />

<link rel="stylesheet" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>vue/css/bootstrap/css/bootstrap.css" type="text/css" />
<!-- <link rel="stylesheet" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>vue/css/bootstrap/js/bootstrap.js"/> -->