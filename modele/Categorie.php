<?php
class Categorie
{
    public static function get_all_categories()
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom 
            FROM categories');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_categorie_by_id($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom 
            FROM categories 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->execute();

        return $req->fetch();
    }

    public static function get_categorie_by_name($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom 
            FROM categories 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->execute();

        return $req->fetch();
    }

    public static function set_categorie($nom)
    {
        global $bdd;

        $req = $bdd->prepare ('
            INSERT INTO categories(nom) 
            VALUES(:nom)');
        $req->bindParam(':nom', $nom, PDO::PARAM_STR, 500);
        $req->execute();
    }

    public static function set_categorie_relation($id_lien, $id_categorie)
    {
        global $bdd;

        $req = $bdd->prepare ('
            INSERT INTO categories_relation(id_lien, id_categorie) 
            VALUES(:id_lien, :id_categorie)');
        $req->bindParam(':id_lien', $id_lien, PDO::PARAM_INT);
        $req->bindParam(':id_categorie', $id_categorie, PDO::PARAM_INT);
        $req->execute();
    }

    public static function delete_categorie_relation($id_lien)
    {
        global $bdd;

        $req = $bdd->prepare ('
            DELETE FROM categories_relation 
            WHERE id_lien = :id_lien');
        $req->bindParam(':id_lien', $id_lien, PDO::PARAM_INT);        
        $req->execute();
    }

    public static function update_categorie($id, $nom)
    {
        global $bdd;

        $req = $bdd->prepare('
            UPDATE categories 
            SET nom = :nom 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->bindParam(':nom', $nom, PDO::PARAM_STR, 500);
        $req->execute();
    }

    public static function delete_categorie($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            DELETE FROM categories 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->execute();
    }

    public static function delete_all_categories()
    {
        global $bdd;

        $req = $bdd->prepare('
            DELETE id, nom 
            FROM categories');
        $req->execute();
    }
}