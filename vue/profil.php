<!DOCTYPE html>
<html>
    <head>
    <?php
		include_once '../vue/template/header.php';
	?>

		<title>Profil</title>
</head>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="http://code.jquery.com/jquery.js"></script>
<script src="../vue/css/bootstrap/js/bootstrap.min.js"></script>

<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-8">
			<?php
				include_once '../vue/template/menu.php';
			?>
		</div>
	</div>
	<div class="row justify-content-center">		
		<body>
			<div class="col-4">  
				<br>
				<h3>Mon profil</h3>
				<hr>

				<h4>Mes informations personnelles</h4>
				<hr>
				<?php
					//affichage des infos utilisateur
					echo "bonjour ".$user['username'];
					echo "<br>";

					echo $user['nom'];
					echo "&nbsp";
					echo $user['prenom'];

					echo "<br>";
					echo $user['email'];
					echo "<br><br>";

					//affichage des liens perso
                    echo "<h3>Mes liens</h3>";
                    

        			foreach ($links as $v1 => $v2) 
                    {
                    echo "<div class='col-12'>";

                    //form + bouton suppression lien
					echo 	"<div class='row'>
							<form method='post' action='' id='supprlien' enctype='multipart/form-data' style='margin:0'>
								<input id='lienIdSuppr' name='lienIdSuppr' type='hidden' value='".$v2['id']."'>
		                		<button type='submit' class='btn btn-danger pull-right btn-sm'>
		                			<i class='material-icons'>delete</i>
		                		</button>
			       			</form>";

			       	//bouton modification lien
			       	echo 	"<a href='#EditionLien".$v2['id']."' role='button' class='btn btn-info btn-sm' data-toggle='modal'>
		                		<i class='material-icons'>create</i>
			       			</a>
			       			</div>";

			       	//affichage lien
					echo 	"<a href='http://".$v2['url']."' target='_blank'>
								<h4>
									<img src='http://".$v2['url']."/favicon.ico' alt='".utf8_encode($v2['lien_nom'])."' height='20' width='20'>  ".utf8_encode($v2['lien_nom'])."
								</h4>
							</a>
							<br>".utf8_encode($v2['categorie_nom'])."
							<br><br>";

						//modal
					echo 	"<div id='EditionLien".$v2['id']."' class='modal hide fade' tabindex='-1' role='dialog' aria-labelledby='EditionLienLabel' aria-hidden='true'>
					  			<div class='modal-header'>
					    			<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
					    			<h3 id='EditionLienLabel'>Edition</h3>
				  				</div>
					  			<div class='modal-body'>

					 				<form method='post' action='' id='formlien' enctype='multipart/form-data'>
		    							<fieldset>
		    								<input id='lienIdEdit' name='lienIdEdit' type='hidden' value='".$v2['id']."'>
						                	<p><input type='text' class='input-xlarge' name='lien_nom'  value='".utf8_encode($v2['lien_nom'])."' required><br></p>
		                                    <p><input type='text' class='input-xlarge' name='url'  value='".$v2['url']."' required></input><br></p>
                            				<div class='multiselect'>";
                            				
                            				//checkox catégories
                                			foreach ($categorie as $v1 => $v2) 
				                            {
												echo "<label><input type='checkbox' name='option[]' value='".$v2['id']."' />".utf8_encode($v2['nom'])."</label>";
				                            }
                    
                    //modal footer        			
					echo 					"</div>
			                				
								 
								 </div>
							  	<div class='modal-footer'>
							  				<p><button type='submit' class='btn btn-success pull-left'>Valider <i class='icon-white icon-ok-sign'></i></button>

				        				</fieldset>
									</form>
								 </div>
							</div>";
					
					//fermeture span4
			       	echo "</div>";
                    }

                ?>

			</div>
		</body>
	</div>
</div>

</html>