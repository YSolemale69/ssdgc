<?php
class User
{
    public static function get_all_user()
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom, prenom, username, email, password, google_id, type 
            FROM user');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_user_by_id($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom, prenom, username, email, password, google_id, type 
            FROM user 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->execute();

        return $req->fetch();
    }

    public static function get_user_by_email($email)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id
            FROM user 
            WHERE email = :email');
        $req->bindParam(':email', $email, PDO::PARAM_STR, 255);
        $req->execute();

        return $req->fetch();
    }

        public static function get_user_by_username($username)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom, prenom, username, email, password, google_id, type 
            FROM user 
            WHERE username = :username');
        $req->bindParam(':username', $username, PDO::PARAM_STR, 255);
        $req->execute();

        return $req->fetch();
    }

    public static function get_user_by_email_password($email, $password)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom, prenom, username, email, password, google_id, type 
            FROM user 
            WHERE email = :email 
            AND password = :password');
        $req->bindParam(':email', $email, PDO::PARAM_STR, 255);
        $req->bindParam(':password', $password, PDO::PARAM_STR, 255);
        $req->execute();

        return $req->fetch();
    }

    public static function get_user_by_google_id($id)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id, nom, prenom, username, email, password, google_id, type 
            FROM user 
            WHERE google_id = :google');
        $req->bindParam(':google', $id, PDO::PARAM_STR, 255);
        $req->execute();

        return $req->fetch();
    }

    public static function get_admin_by_id_email($id, $email)
    {
        global $bdd;

        $req = $bdd->prepare('
            SELECT id 
            FROM admin 
            WHERE user_id = :id 
            AND email = :email');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->bindParam(':email', $email, PDO::PARAM_STR, 255);
        $req->execute();

        return $req->fetch();
    }

    public static function set_user($nom, $prenom, $username, $email, $password, $google_id=null)
    {
        global $bdd;


        $req = $bdd->prepare ('
            INSERT INTO user(nom, prenom, username, email, password, google_id) 
            VALUES(:nom, :prenom, :username, :email, :password, :google)');
        $req->bindParam(':nom', $nom, PDO::PARAM_STR, 255);
        $req->bindParam(':prenom', $prenom, PDO::PARAM_STR, 255);
        $req->bindParam(':username', $username, PDO::PARAM_STR, 255);
        $req->bindParam(':email', $email, PDO::PARAM_STR, 255);
        $req->bindParam(':password', $password, PDO::PARAM_STR, 255);
        $req->bindParam(':google', $google_id, PDO::PARAM_STR, 50);
        $req->execute();
    }

    public static function update_user($id, $nom, $prenom, $username, $email, $google_id=null)
    {
        global $bdd;

        $req = $bdd->prepare ('
            UPDATE user 
            SET nom = :nom, prenom = :prenom, username = :username, email = :email, google_id = :google 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->bindParam(':nom', $nom, PDO::PARAM_STR, 255);
        $req->bindParam(':prenom', $prenom, PDO::PARAM_STR, 255);
        $req->bindParam(':username', $username, PDO::PARAM_STR, 255);
        $req->bindParam(':email', $email, PDO::PARAM_STR, 255);
        $req->bindParam(':google', $google_id, PDO::PARAM_STR, 50);
        $req->execute();
    }

    public static function delete_user($id)
    {
        global $bdd;

        $req = $bdd->prepare ('
            DELETE FROM user 
            WHERE id = :id');
        $req->bindParam(':id', $id, PDO::PARAM_INT);
        $req->execute();
    }
}
