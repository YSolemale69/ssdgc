<?php
session_start();
if (isset($_SESSION['id']))
{
    //sécurisation de l'id
    $_SESSION['id'] = htmlspecialchars(intval($_SESSION['id']));

	header("location:../index.php");
}

// if(password_verify($password, $hashed_password))

//set_include_path(get_include_path() . PATH_SEPARATOR . 'google-api-php-client/src');
$var_dir = "../../";
if (isset($_POST['email'])) 
{
    //sécurisation des données du formulaire
    $_POST['email'] = htmlspecialchars($_POST['email']);
    $_POST['password'] = htmlspecialchars($_POST['password']);

    include_once('../../modele/connexion_sql.php');
    include_once('../../modele/User.php');
    $user_count = User::get_user_by_email($_POST['email']);
    $user_count = User::get_user_by_id($user_count['id']);

    if (isset($user_count['id']) && password_verify($_POST['password'], $user_count['password'])) 
    {
        $_SESSION['id'] = $user_count['id'];
        $_SESSION['email'] = $user_count['email'];
        $_SESSION['username'] = $user_count['username'];
        $_SESSION['type'] = $user_count['type'];
    	header("location:../profil.php");
    }
    else
    {

    	
    	$div_type = "error";
		$erreur_type = "Erreur !";
		$erreur = "Votre email ou votre mot de passe ne correspond pas";
		
    }
	
	include_once '../../vue/auth/login.php';
}
else
{
    // Genere un token et le stock en session
    $state = md5(rand());
    $_SESSION["state"] = $state;
    $client_id = "877013047826-nr9u91tmhe9ubsmtdefm2hglfkfa1c3v.apps.googleusercontent.com";

    $openid_url = "https://accounts.google.com/o/oauth2/auth?client_id={$client_id}&response_type=code&scope=openid%20profile%20email&redirect_uri=http://localhost/ssdgc/google&state={$state}";

	include_once '../../vue/auth/login.php';
}