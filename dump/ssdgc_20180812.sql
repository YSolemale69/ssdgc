-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 12 août 2018 à 19:25
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ssdgc`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`) VALUES
(1, 'Actualités et politique'),
(2, 'Animation'),
(3, 'Animaux'),
(4, 'Auto/Moto'),
(5, 'Cinéma et divertissement'),
(6, 'Cuisine et santé'),
(7, 'Emissions tv'),
(8, 'Humour'),
(9, 'Jeux vidéo et autres'),
(10, 'Mode et beauté'),
(11, 'Musique'),
(12, 'Science et éducation'),
(13, 'Sport'),
(14, 'Technologie');

-- --------------------------------------------------------

--
-- Structure de la table `categories_relation`
--

DROP TABLE IF EXISTS `categories_relation`;
CREATE TABLE IF NOT EXISTS `categories_relation` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_lien` int(15) NOT NULL,
  `id_categorie` int(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categorie` (`id_categorie`),
  KEY `id_lien` (`id_lien`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories_relation`
--

INSERT INTO `categories_relation` (`id`, `id_lien`, `id_categorie`) VALUES
(1, 1, 14),
(8, 6, 1),
(9, 8, 5);

-- --------------------------------------------------------

--
-- Structure de la table `links`
--

DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `lien_nom` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `links`
--

INSERT INTO `links` (`id`, `user_id`, `lien_nom`, `url`, `date`) VALUES
(1, 5, 'Google', 'google.com', '2018-08-04 23:21:54'),
(2, 5, 'Youtube', 'youtube.com', '2018-08-04 23:24:54'),
(6, 5, 'LInkedIn', 'linkedin.com', '2018-08-05 22:58:18'),
(7, 5, 'Linkedin', 'Linkedin.com', '2018-08-06 21:42:30'),
(8, 5, 'Deezer', 'deezer.com', '2018-08-10 21:56:06');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `google_id` varchar(50) DEFAULT NULL,
  `type` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `username`, `email`, `password`, `google_id`, `type`) VALUES
(5, 'SolÃ©malÃ©', 'Yannis', 'kibi', 'shinobyan@gmail.com', '8a84118f8773584bcc1eae815bba6e701cfce175', NULL, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `categories_relation`
--
ALTER TABLE `categories_relation`
  ADD CONSTRAINT `categories_relation_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
