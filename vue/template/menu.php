<nav class="navbar navbar-expand-lg">
		<ul class="navbar-nav mr-auto">

			<li class="nav-item active"><a class="nav-link" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>index.php">accueil</a></li>

			<?php if(isset($_SESSION['id'])){ ?>
			<li class="nav-item"><a class="nav-link" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>controleur/profil.php">profil</a></li>
			<li class="nav-item"><a class="nav-link" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>controleur/lien.php">lien</a></li>
			<li class="nav-item"><a class="nav-link" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>controleur/logout.php">Se déconnecter</a></li>

			<?php }else{ ?>
			<li class="nav-item"><a class="nav-link" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>controleur/auth/login.php">login</a></li> 
			<li class="nav-item"><a class="nav-link" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>controleur/auth/register.php">register</a></li>
			<?php } ?>

		</ul>
</nav>
